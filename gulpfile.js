//https://www.sitepoint.com/simple-gulpy-workflow-sass/
var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var postcss = require('gulp-postcss');
var lost = require('lost');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var cssmin = require('gulp-minify-css');
var kraken = require('gulp-kraken');
var clean = require('gulp-clean');
var copy = require('gulp-copy');
var sequence = require('gulp-sequence');
var watch = require('gulp-watch');
var pug = require('gulp-pug');
var browserSync = require('browser-sync').create();
var extReplace = require('gulp-ext-replace');

var input = './src/sass/styles.scss';
var output = './build/css';

var sassOptions = {
    errLogToConsole: true,
    outputStyle: 'expanded'
};

var pugOptions = {
    // input: './src/pug/**/*.pug',
    input: ['./src/pug/**/*.pug', '!./src/pug/mixins/*.pug'],
    output: './build'
};

gulp.task('pug', function buildHTML() {
    return gulp
        .src(pugOptions.input)
        .pipe(pug())
        .on('error', function (err) {
            console.log(err.toString());
            this.emit('end');
        })
        .pipe(gulp.dest(pugOptions.output));
});

gulp.task('sass', function () {
    return gulp
        .src(input)
        .pipe(sourcemaps.init())
        .pipe(sass(sassOptions).on('error', sass.logError))
        .pipe(postcss([
            lost()
        ]))
        .pipe(autoprefixer())
        /*.pipe(cssmin({
            compatibility: '*,-units.pt,-units.ch,-units.in,-units.pc,-units.pt,-units.rem',
            advanced: false,
            aggressiveMerging: false,
            restructuring: false,
            roundingPrecision: -1
        }))*/
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(output));
});

gulp.task('js', function () {
    return gulp.src(['./src/js/**/*.js'])
    .pipe(concat('main.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./build/js/'));
});

gulp.task('clean css', function () {
    return gulp.src(['./build/css'])
        .pipe(clean());
});

gulp.task('kraken', function () {
    gulp.src('./build/img/cases/promo/**')
        .pipe(kraken({
            key: 'efd408bc531bf8a6c8d0d655d8c1f3cf',
            secret: 'fa7dd182150febc248935b952e96a659eca5599d',
            lossy: true,
            concurrency: 6
        }));
});

gulp.task('replace .html extension', function() {
    gulp.src('./build//*.html')
        .pipe(extReplace(''))
        .pipe(gulp.dest('./build'))
});

gulp.task('WATCH - browserSync', function() {
    browserSync.init({
        server: {
            baseDir: "./build",
            startPath: 'index',
            serveStaticOptions: {
                extensions: ['html'] // This is the trick!
            }
        },
        ui: false,
        minify: false,
        notify: false,
        timestamps: false,
        ghostMode: false
    });

    gulp.watch("./src/sass/**/*.scss", ['sass']);
    gulp.watch("./src/pug/**/*.pug", ['pug']);
    gulp.watch("./src/js/**/*.js", ['js']);
    gulp.watch("./build/css/**/*.css").on('change', browserSync.reload);
    gulp.watch("./build/*.html").on('change', browserSync.reload);
});

gulp.task('default', ['WATCH - browserSync']);
gulp.task('build', ['pug', 'sass', 'js']);