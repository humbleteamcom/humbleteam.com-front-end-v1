$(document).ready(function () {
    var strAr = ['with', 'at', 'or', 'from', 'into', 'of', 'to', 'in', 'for', 'on', 'by', 'over', 'but', 'up', 'out', 'down', 'off', 'near'];
    var str1 = '';
    var str2 = '';

    // console.log(strAr.length);

    for (var i = 0; i < strAr.length; i++) {
        str1 = strAr[i];
        str2 = '\\b' + str1 + '\\b';
        // console.log(str1);

        document.body.innerHTML = document.body.innerHTML.replace(new RegExp(str2 + ' ', 'gi'), `${str1}&nbsp;`);
    }
});