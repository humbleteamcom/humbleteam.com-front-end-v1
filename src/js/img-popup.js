$(document).ready(function () {
    function popupOpen(e) {
        var _self = $(e.currentTarget);
        var winWidth;
        var imgWidth;

        $('.js-img-popup-img').attr('src', _self.attr('src'));
        if (_self.attr('srcset')) {
            $('.js-img-popup-img').attr('srcset', _self.attr('srcset'));
        }

        setTimeout(function () {
            winWidth = window.innerWidth;
            imgWidth = $('.js-img-popup-img').width();

            $('.js-img-popup-img-wrap').scrollLeft((imgWidth - winWidth) / 2);
            $('html').addClass('_img-popup');
        }, 150);
    }

    function popupClose(e) {
        $('html').removeClass('_img-popup');
    }

    $(window).on('resize', $.debounce(200, false, function () {
        $('.js-popup-img').off('click');
        $('#img-popup-close').off('click');

        if (window.innerWidth < 600) {
            $('.js-popup-img').click(popupOpen);
            $('#img-popup-close').click(popupClose);
        } else {
            popupClose();
        }
    }));

    $(window).resize();
});