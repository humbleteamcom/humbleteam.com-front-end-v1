$(document).ready(function () {
    var header = $('#header');
    var firstScreen = $('.js-first-screen');
    var firstScreenHeight = firstScreen.height() - 45;

    if (firstScreen.length) {
        $(window).on('resize', $.debounce(200, false, function () {
            firstScreenHeight = firstScreen.height() - 45;
        }));

        // $(window).on('scroll', $.debounce(100, false, function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > firstScreenHeight) {
                header.addClass('_always-dark');
            } else {
                header.removeClass('_always-dark');
            }
        });
        // }));
    } else {
        header.addClass('_always-dark');
    }
});