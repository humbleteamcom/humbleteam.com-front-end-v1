$(document).ready(function () {
    var wow = new WOW({
        boxClass: 'js-wow',
        animateClass: 'animated',
        offset: 0,
        mobile: true,
        live: false,
        scrollContainer: null
    });
    wow.init();
});