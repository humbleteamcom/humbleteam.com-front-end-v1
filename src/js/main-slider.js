$(document).ready(function () {
    var promoSlider = new Swiper('.js-index-promo-slider-cont', {
        autoplay: {
            delay: 6000,
            disableOnInteraction: false
        },
        loop: true,
        speed: 750,
        effect: 'fade',
        fadeEffect: {
            crossFade: true
        },
        setWrapperSize: true,
        roundLengths: false,
        watchSlidesVisibility: false,
        preloadImages: true,
        wrapperClass: 'js-index-promo-slider-wrap',
        slideClass: 'js-index-promo-slider-item',
        // resistance: '100%',
        // shortSwipes: false,
        // keyboard: {
        //     enabled: false
        // },
        // navigation: {
        //     prevEl: '.js-index-promo-slider-prev',
        //     nextEl: '.js-index-promo-slider-next'
        // },
        pagination: {
            el: '.js-index-promo-pagination',
            clickable: true
        },
        followFinger: false,
        simulateTouch: false,
        preventInteractionOnTransition: true,
        runCallbacksOnInit: true,
        on: {
            init: function () {
                $('.js-index-promo-slider').removeClass('_hidden');

                if (!$('#header').hasClass('_always-dark')) {
                    if ($('.js-index-promo-slider-item').not('.swiper-slide-duplicate').eq(0).attr('data-header') === 'dark') {
                        $('#header').addClass('header_dark');
                        $('.js-index-promo-slider').addClass('white_paging');
                    } else {
                        $('#header').removeClass('header_dark');
                        $('.js-index-promo-slider').removeClass('white_paging');
                    }
                }
            },
            transitionStart: function () {
                if (promoSlider) {
                    if (promoSlider.slides.eq(promoSlider.activeIndex).attr('data-header') === 'dark') {
                        $('#header').addClass('header_dark');
                        $('.js-index-promo-slider').addClass('white_paging');
                    } else {
                        $('#header').removeClass('header_dark');
                        $('.js-index-promo-slider').removeClass('white_paging');
                    }
                }
            },
            // transitionStart: this.on_trans_start.bind(this)
            // transitionStart: this.on_trans_start.bind(this)
            // transitionEnd: this.on_trans_end.bind(this),
        }
    });

    $('.js-slide-cursor-left').click(function () {
        promoSlider.slidePrev();
    });

    $('.js-slide-cursor-right').click(function () {
        promoSlider.slideNext();
    });
});